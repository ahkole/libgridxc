# -*- Autoconf -*-
#
# Copyright (C) 2015 Yann Pouillon
#
# This file is part of the LibGridXC software package. For license information,
# please see the COPYING file in the top-level directory of the LibGridXC source
# distribution.
#



# GXC_MPI_DETECT()
# ----------------
#
# Chechks whether the configured MPI implementation is working.
#
AC_DEFUN([GXC_MPI_DETECT], [
  dnl Init
  gxc_mpi_ok="unknown"

  dnl Display current MPI status
  AC_MSG_CHECKING([how MPI parameters have been set])
  AC_MSG_RESULT([${gxc_mpi_type}])
  AC_MSG_CHECKING([whether the MPI C compiler is set])
  AC_MSG_RESULT([${gxc_mpi_cc_set}])
  AC_MSG_CHECKING([whether the MPI Fortran compiler is set])
  AC_MSG_RESULT([${gxc_mpi_fc_set}])

  dnl Test MPI compilers
  _GXC_MPI_CHECK_CC([${CC}])
  if test "${gxc_mpi_cc_ok}" = "yes"; then
    _GXC_MPI_CHECK_FC([${FC}])
  fi

  dnl Take final decision
  AC_MSG_CHECKING([whether we have a full MPI support])
  if test "${gxc_mpi_cc_ok}" = "yes" -a \
          "${gxc_mpi_fc_ok}" = "yes"; then
    gxc_mpi_ok="yes"
  else
    gxc_mpi_ok="no"
  fi
  AC_MSG_RESULT([${gxc_mpi_ok}])
]) # GXC_MPI_DETECT



# GXC_MPI_INIT()
# --------------
#
# Initializes MPI parameters.
#
AC_DEFUN([GXC_MPI_INIT], [
  gxc_mpi_ok="unknown"
  gxc_mpi_cc_ok="unknown"
  gxc_mpi_fc_ok="unknown"

  if test "${gxc_mpi_enable}" != "no"; then
    AC_MSG_CHECKING([how MPI parameters have been set])
    AC_MSG_RESULT([${gxc_mpi_type}])
    _GXC_MPI_INIT_CC
    _GXC_MPI_INIT_FC
  fi
]) # GXC_MPI_INIT



                    ########################################



# _GXC_MPI_CHECK_CC(CC)
# ---------------------
#
# Check whether the MPI C compiler is working.
#
AC_DEFUN([_GXC_MPI_CHECK_CC], [
  dnl Init
  gxc_mpi_cc_ok="unknown"
  gxc_mpi_cc_has_funs="unknown"
  gxc_mpi_cc_has_hdrs="unknown"
  tmp_mpi_cc_name=`basename "$1"`

  dnl Make sure MPI-related cached values are wiped out
  tmp_mpi_header="mpi.h"
  tmp_mpi_cache=AS_TR_SH([ac_cv_header_${tmp_mpi_header}])
  eval ${as_unset} ${tmp_mpi_cache}
  tmp_mpi_func="MPI_Init"
  tmp_mpi_cache=AS_TR_SH([ac_cv_func_${tmp_mpi_func}])
  eval ${as_unset} ${tmp_mpi_cache}

  dnl Prepare environment
  gxc_saved_CC="${CC}"
  CC="$1"

  dnl Look for C includes
  AC_LANG_PUSH([C])
  AC_CHECK_HEADERS([mpi.h],
    [gxc_mpi_cc_has_hdrs="yes"], [gxc_mpi_cc_has_hdrs="no"])
  AC_LANG_POP([C])

  dnl Look for C functions
  if test "${gxc_mpi_cc_has_hdrs}" = "yes"; then
    AC_CHECK_FUNC([MPI_Init], [gxc_mpi_cc_has_funs="yes"],
      [gxc_mpi_cc_has_funs="no"])
  fi

  dnl Validate C support
  AC_MSG_CHECKING([whether the C compiler (${tmp_mpi_cc_name}) has MPI])
  if test "${gxc_mpi_cc_has_funs}" = "yes" -a \
          "${gxc_mpi_cc_has_hdrs}" = "yes"; then
    gxc_mpi_cc_ok="yes"
  else
    gxc_mpi_cc_ok="no"
  fi
  AC_MSG_RESULT([${gxc_mpi_cc_ok}])

  dnl Restore environment
  CC="${gxc_saved_CC}"
  unset tmp_mpi_cache
  unset tmp_mpi_cc_name
  unset tmp_mpi_func
  unset tmp_mpi_header
]) # _GXC_MPI_CHECK_CC



# _GXC_MPI_CHECK_FC(FC)
# ---------------------
#
# Check whether the MPI Fortran compiler is working.
#
AC_DEFUN([_GXC_MPI_CHECK_FC], [
  dnl Init
  gxc_mpi_fc_ok="unknown"
  gxc_mpi_fc_has_funs="unknown"
  gxc_mpi_fc_has_mods="unknown"
  tmp_mpi_fc_name=`basename "$1"`

  dnl Make sure MPI-related cached values are wiped out
  tmp_mpi_header="mpi.h"
  tmp_mpi_cache=AS_TR_SH([ac_cv_header_${tmp_mpi_header}])
  eval ${as_unset} ${tmp_mpi_cache}
  tmp_mpi_func="MPI_Init"
  tmp_mpi_cache=AS_TR_SH([ac_cv_func_${tmp_mpi_func}])
  eval ${as_unset} ${tmp_mpi_cache}

  dnl Prepare environment
  gxc_saved_FC="${FC}"
  FC="$1"

  dnl Look for Fortran modules
  AC_LANG_PUSH([Fortran])
  AC_MSG_CHECKING([for a Fortran MPI module])
  AC_LINK_IFELSE([AC_LANG_PROGRAM([],
    [[
      use mpi
    ]])], [gxc_mpi_fc_has_mods="yes"], [gxc_mpi_fc_has_mods="no"])
  AC_MSG_RESULT([${gxc_mpi_fc_has_mods}])
  AC_LANG_POP([Fortran])

  dnl Look for Fortran functions
  if test "${gxc_mpi_fc_has_mods}" = "yes"; then
    AC_LANG_PUSH([Fortran])
    AC_MSG_CHECKING([for a Fortran MPI_Init])
    AC_LINK_IFELSE([AC_LANG_PROGRAM([],
      [[
        use mpi
        integer :: ierr
        call mpi_init(ierr)
      ]])], [gxc_mpi_fc_has_funs="yes"], [gxc_mpi_fc_has_funs="no"])
    AC_MSG_RESULT([${gxc_mpi_fc_has_funs}])
    AC_LANG_POP([Fortran])
  fi

  dnl Validate Fortran support
  AC_MSG_CHECKING([whether the Fortran compiler (${tmp_mpi_fc_name}) has MPI])
  if test "${gxc_mpi_fc_has_funs}" = "yes" -a \
          "${gxc_mpi_fc_has_mods}" = "yes"; then
    gxc_mpi_fc_ok="yes"
  else
    gxc_mpi_fc_ok="no"
  fi
  AC_MSG_RESULT([${gxc_mpi_fc_ok}])

  dnl Restore environment
  FC="${gxc_saved_FC}"
  unset tmp_mpi_cache
  unset tmp_mpi_fc_name
  unset tmp_mpi_func
  unset tmp_mpi_header
]) # _GXC_MPI_CHECK_FC



# _GXC_MPI_INIT_CC()
# ------------------
#
# Initializes MPI parameters related to the C compiler.
#
AC_DEFUN([_GXC_MPI_INIT_CC], [
  dnl Init
  gxc_sercc="${CC}"
  gxc_mpicc=""
  gxc_mpi_cc_set="no"

  AC_MSG_NOTICE([initializing MPI C parameters])

  dnl Look for a MPI C compiler
  case "${gxc_mpi_type}" in

    pfx)
      gxc_mpicc="${with_mpi}/bin/mpicc"
      AC_MSG_CHECKING([for an executable MPI C compiler])
      if test -x "${gxc_mpicc}"; then
        AC_MSG_RESULT([${gxc_mpicc}])
        if test "${gxc_sercc}" = ""; then
          AC_MSG_NOTICE([setting CC to '${gxc_mpicc}'])
          CC="${gxc_mpicc}"
          gxc_mpi_cc_set="yes"
        fi
      else
        AC_MSG_RESULT([none])
        AC_MSG_ERROR([MPI C compiler not found in ${with_mpi}/bin])
      fi
      ;;

    def|yon)
      if test -z "${CC}"; then
        AC_CHECK_PROGS([gxc_mpicc], [mpicc])
        if test ! -z "${gxc_mpicc}"; then
          AC_MSG_NOTICE([setting CC to '${gxc_mpicc}'])
          CC="${gxc_mpicc}"
          gxc_mpi_cc_set="yes"
        fi
      fi
      ;;

  esac
]) # _GXC_MPI_INIT_CC



# _GXC_MPI_INIT_FC()
# ------------------
#
# Initializes MPI parameters related to the Fortran compiler.
#
AC_DEFUN([_GXC_MPI_INIT_FC], [
  dnl Init
  gxc_serfc="${FC}"
  gxc_mpifc=""
  gxc_mpi_fc_set="no"

  AC_MSG_NOTICE([initializing MPI Fortran parameters])

  dnl Look for a MPI Fortran compiler
  case "${gxc_mpi_type}" in

    pfx)
      gxc_mpifc="${with_mpi}/bin/mpif90"
      if test -x "${gxc_mpifc}"; then
        AC_MSG_CHECKING([for an executable MPI Fortran compiler])
        AC_MSG_RESULT([${gxc_mpifc}])
        if test "${gxc_serfc}" = ""; then
          AC_MSG_NOTICE([setting FC to '${gxc_mpifc}'])
          FC="${gxc_mpifc}"
          gxc_mpi_fc_set="yes"
        fi
      else
        AC_MSG_ERROR([MPI Fortran compiler not found in ${with_mpi}/bin])
      fi
      ;;

    def|yon)
      if test -z "${FC}"; then
        AC_CHECK_PROGS([gxc_mpifc], [mpifort mpif90 mpif95])
        if test ! -z "${gxc_mpifc}"; then
          AC_MSG_NOTICE([setting FC to '${gxc_mpifc}'])
          FC="${gxc_mpifc}"
          gxc_mpi_fc_set="yes"
        fi
      fi
      ;;

  esac
]) # _GXC_MPI_INIT_FC

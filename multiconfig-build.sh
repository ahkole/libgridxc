#!/bin/sh

# Helper script to install the full range of flavors of libgridxc (assuming libxc functionality).
# It uses the '--enable-multiconfig' feature to tag the different flavors appropriately in the
# installation directory.

# These variables should be defined, either through environmental variables or as script arguments.

#GRIDXC_PREFIX=/tmp/GXC_MULTI

#CC=gcc
#FC=gfortran
#MPIFC=mpif90
#MPICC=mpicc

#LIBXC_ROOT  # Installation location of libxc.
#MPI_ROOT    # Installation location of the mpi framework.
if test "x$MPI_ROOT" != "x" ; then
    _config_mpi="--with-mpi=$MPI_ROOT"
else
    _config_mpi="--with-mpi"
fi

# Default options
_with_mpi=1
if test "x$MPIFC" = "x" ; then
    echo "Will not build MPI variants!"
    _with_mpi=0
fi

# Now parse options from user
while test $# -gt 0 ; do
    opt=$1 ; shift
    case $opt in
	--prefix|-p)
	    GRIDXC_PREFIX=$1 ; shift
	    ;;
	--libxc-root)
	    LIBXC_ROOT=$1 ; shift
	    ;;
	--with-mpi)
	    _with_mpi=1
	    ;;
	--without-mpi)
	    _with_mpi=0
	    ;;
	--help|-h)
	    echo " $0 --help shows this message"
	    echo ""
	    echo "These options are available:"
	    echo ""
	    echo "  --prefix|-p <>: specify the installation directory of libgridxc"
	    echo "  --libxc-root <>: specify directory where libxc is installed (default: $LIBXC_ROOT)"
	    echo "  --with-mpi: force MPI builds, requires MPIFC and MPICC are set"
	    echo "  --without-mpi: do not build MPI libraries"
	    echo "  --help|-h: show this help"
	    echo ""
	    exit 0
	    ;;
    esac
done


if [ "x$GRIDXC_PREFIX" = "x" ]; then
    echo "You have not specified an installation directory (GRIDXC_PREFIX)"
    echo "Please provide an installation directory:"
    echo "  GRIDXC_PREFIX=<directory> $0"
    echo "or"
    echo "  $0 --prefix <directory>"
    exit 2
fi

if [ "x$LIBXC_ROOT" = "x" ]; then
    echo "You have not specified a libxc installation directory (LIBXC_ROOT)"
    echo "Please provide a directory of a previous libxc installation directory:"
    echo "  LIBXC_ROOT=<directory> $0"
    echo "or"
    echo "  $0 --libxc-root <directory>"
    exit 3
fi


# ensure libtool does not grab a wrong env-var
unset F77
unset F90

##########
# Double #
#   MPI  #
##########
if test $_with_mpi -eq 1 ; then
    rm -rf build_dp_mpi; mkdir build_dp_mpi; cd build_dp_mpi
    FC=$MPIFC CC=$MPICC ../configure --enable-multiconfig $_config_mpi --with-libxc=${LIBXC_ROOT} --prefix=${GRIDXC_PREFIX}
    test $? -ne 0 && exit 1
    make
    test $? -ne 0 && exit 1
    make check > gridxc_dp_mpi.check
    test $? -ne 0 && exit 1
    make install
    test $? -ne 0 && exit 1
    mv gridxc_dp_mpi.check $GRIDXC_PREFIX/
    cd ..
fi

##########
# Double #
##########
rm -rf build_dp; mkdir build_dp; cd build_dp
../configure --enable-multiconfig --without-mpi --with-libxc=${LIBXC_ROOT} --prefix=${GRIDXC_PREFIX}
test $? -ne 0 && exit 1
make
test $? -ne 0 && exit 1
make check > gridxc_dp.check
test $? -ne 0 && exit 1
make install
test $? -ne 0 && exit 1
mv gridxc_dp.check $GRIDXC_PREFIX/
cd ..

##########
# Single #
#   MPI  #
##########
if test $_with_mpi -eq 1 ; then
    rm -rf build_sp_mpi; mkdir build_sp_mpi; cd build_sp_mpi
    FC=$MPIFC CC=$MPICC ../configure --enable-multiconfig $_config_mpi --enable-single-precision --with-libxc=${LIBXC_ROOT} --prefix=${GRIDXC_PREFIX}
    test $? -ne 0 && exit 1
    make
    test $? -ne 0 && exit 1
    make check > gridxc_sp_mpi.check
    test $? -ne 0 && exit 1
    make install
    test $? -ne 0 && exit 1
    mv gridxc_sp_mpi.check $GRIDXC_PREFIX/
    cd ..
fi

##########
# Single #
##########
rm -rf build_sp ; mkdir build_sp; cd build_sp
../configure --enable-multiconfig --without-mpi --enable-single-precision --with-libxc=${LIBXC_ROOT} --prefix=${GRIDXC_PREFIX}
test $? -ne 0 && exit 1
make
test $? -ne 0 && exit 1
make check > gridxc_sp.check
test $? -ne 0 && exit 1
make install
test $? -ne 0 && exit 1
mv gridxc_sp.check $GRIDXC_PREFIX/
cd ..

